# IRIS Systems Recruitments 2024

This is a submission to the IRIS Sytems Recruitments 2024 Tasks

This project consists of several tasks, each in its own branch. Each branch contains a Readme.md file where all the steps have been documented. None of the tasks have been merged onto the main branch. At the moment, [Bonus Task: Kubernetes](https://gitlab.com/iris_recs/231cs158/-/tree/bonuskubernetes) is the latest working branch. It is recommended to `git checkout` to the latest working branch while running it locally since the working of every individual branch hasn't been tested yet.

Swaraj Singh  
231CS158  


## Completed Tasks
- [Task 1](https://gitlab.com/iris_recs/231cs158/-/tree/task1)
- [Task 2](https://gitlab.com/iris_recs/231cs158/-/tree/task2)
- [Task 3](https://gitlab.com/iris_recs/231cs158/-/tree/task3)
- [Task 4](https://gitlab.com/iris_recs/231cs158/-/tree/task4)
- [Task 5](https://gitlab.com/iris_recs/231cs158/-/tree/task5)
- [Task 6](https://gitlab.com/iris_recs/231cs158/-/tree/task6) (was done after Task 7)
- [Task 7](https://gitlab.com/iris_recs/231cs158/-/tree/task7)
- [Bonus Task: CI CD Pipeline](https://gitlab.com/iris_recs/231cs158/-/tree/gitlabpipeline)
- [Bonus Task: Kubernetes](https://gitlab.com/iris_recs/231cs158/-/tree/bonuskubernetes) (latest working branch) (was completed after due date)

## Tasks currently working on

- [Task 8]()
- [Share storage on a separate container among all Rails instances]()

## Relevant Issues
 - 

## Other Bugs in App
- The app breaks if an image hasn't been uploaded while creating a task. 
- While viewing a task, the image uploaded for the task is not rendered everytime.

## Current Architecture


## Latest Working Branch App for Reference
![Image 0](/documentation_images/task6_0.png)
![Image 1](/documentation_images/task6_1.png)
![Image 2](/documentation_images/task6_2.png)
![Image 3](/documentation_images/task6_3.png)
![Image 4](/documentation_images/task6_4.png)
